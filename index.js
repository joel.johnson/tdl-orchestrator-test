const express = require("express");
const app = express();
const port = 3000;

const { exec } = require("child_process");

app.get("/", (req, res) => {
    console.log("hello")
});

app.get("/orchestrator", (req, res) => {
    exec("bf", (error, stdout, stderr) => {
        if (error) console.log(error);
        if (stdout) console.log(stdout);
        if (stderr) console.log(stderr);
        console.log("running refresh");
    });
});

app.listen(port, () => {
    console.log("listening on port 3000");
});